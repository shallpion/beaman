package beaman;

import java.awt.EventQueue;
import javax.swing.JFrame;

public class BeAMan extends JFrame {

    public BeAMan() {
        
        initUI();
    }
    
    private void initUI() {
        
        add(new Board());
        
        setSize(800, 600);
        setResizable(false);
        
        setTitle("是男人就坚持20秒");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                
                BeAMan bm = new BeAMan();
                bm.setVisible(true);
            }
        });
    }
}
