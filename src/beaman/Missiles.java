package beaman;

import java.util.Random;
import java.lang.Math;

public class Missiles {
    private double dx;
    private double dy;
    private double speed = 1.5;
    private double directionX;
    private double directionY;
    
    private double x;
    private double y;
    private int radius;

    private Random rdm;

    public Missiles() {

        initMissiles();
    }

    private void initMissiles() {
        rdm = new Random();
        x = rdm.nextInt(796) + 2;
        y = rdm.nextInt(596) + 2;
        directionX = 2*rdm.nextDouble() - 1;
        //directionY = 2*rdm.nextDouble() - 1;
        directionY = Math.sqrt(1-directionX*directionX);
        dx = speed * directionX;
        dy = speed * directionY;
    //    dx = 1;
     //   dy = -1;
    }

    public void move() {
        x += dx;
        if (x <= 0 || x >= 784 )
            dx = -dx;

        y += dy;
        if (y <= 0 || y >= 554 )
            dy = -dy;
    }

    public int getX() {
        return (int)x;
    }

    public int getY() {
        return (int)y;
    }
}
