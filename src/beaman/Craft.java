package beaman;

import java.awt.Image;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import java.net.URL;
public class Craft {

    private int dx;
    private int dy;
    private int x;
    private int y;
    private Image image;

    public Craft() {

        initCraft();
    }

    private void initCraft() {

        java.net.URL imgURL = getClass().getClassLoader().getResource("images/craft.png");
        ImageIcon ii = new ImageIcon(imgURL);
        image = ii.getImage();
        x = 368;
        y = 268;        
    }


    public void move() {
        if ( (x>=0 && x<=768) || (x<=0 && dx >0) || (x>=768 && dx<0) )
            x += dx;
        if (y>=0 && y<=568 || (y<=0 && dy >0) || (y>= 568 && dy<0) )
            y += dy;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Image getImage() {
        return image;
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_A) {
            dx = -1;
        }

        if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D) {
            dx = 1;
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_W) {
            dy = -1;
        }

        if (key == KeyEvent.VK_DOWN || key == KeyEvent.VK_S) {
            dy = 1;
        }
    }

    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_A) {
            dx = 0;
        }

        if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D) {
            dx = 0;
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_W) {
            dy = 0;
        }

        if (key == KeyEvent.VK_DOWN || key == KeyEvent.VK_S) {
            dy = 0;
        }
    }
}
