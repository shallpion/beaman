package beaman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Font;
import java.awt.FontMetrics;
import javax.swing.JPanel;
import javax.swing.Timer;


public class Board extends JPanel implements ActionListener {

    private Timer timer;
    private Craft craft;
    private Missiles [] missiles;
    private int N=50;   // no of missiles
    private final int DELAY = 10;
    private double duration;


    private boolean startGame;
    private boolean gameStopped;
    public Board() {

        initBoard();
    }


    private void initBoard() {

        startGame = false; 
        gameStopped = false;
        duration = 0;

        addKeyListener(new TAdapter());
        setFocusable(true);
        setBackground(Color.BLACK);

        craft = new Craft();
        missiles = new Missiles[N];
        for (int i=0; i<N; i++)
            missiles[i] = new Missiles();


        timer = new Timer(DELAY, this);
        timer.start();        
    }
    private void resetGame() {

        gameStopped = false;
        startGame = false; 
        duration = 0;


        craft = new Craft();
        missiles = new Missiles[N];
        for (int i=0; i<N; i++)
            missiles[i] = new Missiles();


    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        doDrawing(g);

        Toolkit.getDefaultToolkit().sync();
    }

    private void doDrawing(Graphics g) {

        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(craft.getImage(), craft.getX(), craft.getY(), this);        

        g.setColor(Color.orange);

        for (int i=0; i<N; i++)
            g2d.fillOval(missiles[i].getX(), missiles[i].getY(), 4, 4);
        //  g2d.fillOval(missiles1.getX(), missiles1.getY(), 4, 4);

        if(!startGame) {
            g.setColor(Color.white);
            Font currentFont = g.getFont();
            Font newFont = currentFont.deriveFont(currentFont.getSize() * 3.0F);
            g.setFont(newFont);

            FontMetrics fontMetrics = g.getFontMetrics();
            String s = "Press space when you are ready!";
            int stringX = (790 - fontMetrics.stringWidth(s)) / 2;
            g2d.drawString(s, stringX, 400);
        }

        if(gameStopped) {
            g.setColor(Color.red);
            Font currentFont = g.getFont();
            Font newFont = currentFont.deriveFont(currentFont.getSize() * 20.0F);
            g.setFont(newFont);

            FontMetrics fontMetrics1 = g.getFontMetrics();
            String s1 = duration/100 + "s";
            int stringX1 = (790 - fontMetrics1.stringWidth(s1)) / 2;


            g2d.drawString(s1, stringX1, 300);

            newFont = currentFont.deriveFont(currentFont.getSize() * 3.0F);
            g.setFont(newFont);

            FontMetrics fontMetrics2 = g.getFontMetrics();
            String s2 = "Press space to restart";
            int stringX2 = (790 - fontMetrics2.stringWidth(s2)) / 2;

            g2d.drawString(s2, stringX2, 400);
        }

    }

    public boolean hit(int i) {
        int X = missiles[i].getX();
        int Y = missiles[i].getY();
        int cX = craft.getX();
        int cY = craft.getY();

        if ((X >= cX && X <= cX + 20) && (Y>=cY && Y<=cY+30))
            return true;
        else 
            return false;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (startGame && !gameStopped) {
            duration += 1;
            craft.move();
            for (int i=0; i<N; i++) { 
                missiles[i].move();
                if (hit(i)) {
                    gameStopped = true;
                }
            }
        }


        repaint();  
    }

    private class TAdapter extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent e) {
            craft.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {
            craft.keyPressed(e);

            int key = e.getKeyCode();
            if (key == KeyEvent.VK_SPACE) {
                if(startGame == false) {
                    startGame = true;
                }
                else if (gameStopped == true) {
                    startGame = false;
                    resetGame();
                }

            }
        }
    }
}
